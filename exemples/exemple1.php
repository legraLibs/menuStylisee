<?php
//*********** ==== Gestion des pages et des menus ==== *****************

include '../lib/legral/php/gestLib/gestLib-0.1.php';
include '../menuStylisee.php';

// --  creation du menu
$menu=new menuStylisee('ms');

// -- ajout des entrees du menu-- //

//addLi     ($index,	$titre,										$get,		$urlL,				$cssL		, inlineLI,			,inlneA){
$menu->addLi('libLoad', 'lib chargéesl'                                          			, NULL		,'./exemples/libLoad.php');
$menu->addLi('li1',	'titre1 avec titre,sans get, sans url'	);
$menu->addLi('li11',    'titr11 avec titre,avec get, sans url'						, 'page=page11');
$menu->addLi('page1',	'titre1 appel page1.html'							, NULL		,'./page1.html');
$menu->addLi('page2',   'titre2 appel page2.html'							, NULL          ,'./page2.html');


$menu->addLi('li3',     'titre3 avec titre avec get sans url avec css'					, 'page=li3'	, NULL	       			,'classe3');
$menu->addCSSLI('li3','dossierIcone');

$menu->addLi('li4',     'titre4 avec titre sans get sans url sans css avec inlineLI'			, NULL  	,NULL           		,NULL   ,"onclick=\"alert('alert4');\""	,NULL		  );
$menu->addLi('li5',	'titre5 avec titre sans get avec url sans css sans inlineLI avec inlineA  '	, NULL		,'./page5.html'			,NULL	,"onclick=\"alert('alert5');\""	,"target='_blank'");
$menu->addCSSA('li5','dossier1');

$menu->addLi('top',     'titre5 avec titre sans get avec url sans css sans inlineLI avec inlineA  '	, NULL		,'#top' 			,NULL   ,"onclick=\"alert('au top!');\"",NULL);
$menu->addLi('na',      'url inexistante'                                                              , NULL           ,'./inexistant.php');
//*********************** ==== html ==== *****************
?><!DOCTYPE html><html lang="fr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta name="robots" content="index,follow">
<meta name="author" content="Pascal TOLEDO">
<meta name="description" content="">
<meta name="keywords"content="">
<meta name="generator" content="vim">
<meta name="identifier-url" content="http://legral.fr">
<meta name="date-creation-yyyymmdd" content="20140508">
<meta name="date-update-yyyymmdd" content="201400508">
<meta name="reply-to" content="pascal.toledo@legral.fr">
<meta name="revisit-after" content="10 days">
<meta name="category" content="">
<meta name="publisher" content="legral.fr">
<meta name="copyright" content="pascal TOLEDO">

<title></title>

<link rel="stylesheet" href="../intersites.css" type="text/css" media="all" />
<link rel="stylesheet" href="../intersites/styles/html4/style1.css" type="text/css" media="all" />

<!-- scc specifique au menu -->
<link rel="stylesheet" href="../styles/menuOnglets-defaut/menu.css" media="all" />


<style type="text/css"></style>
</head>
<body>
<div id="page">

<h1>lib: menuStylisee</h1>

<p>Cette librairie contruit un menu selon le template appelle. Il n'apelle pas les page associé au menu. Il faut utilisé pour cela la librairie gestMenu ou gestPage. Les 2 utilise menuStylisee par defaut.</p>

<p>instanciation d'un menu:<br>
$menu=new menuStylisee('ms');<br>
ms: nom de la classe appellé par le menu

<h2>menu1</h2>
<p>On affiche le menu</p>
<?php 
$page=$_GET['page'];
// - affiche le menu avec l'entree du menu correspondant a $page en etat selectionne - //
echo $menu->show($page);
?>

Nous revoila!

<!--
<p>Contenu de l'objet menu</p>
<?php echo $menu;?>
-->


<hr>
<h2>menu2</h2>

<?php
$menu2=new menuStylisee('menuSansStyle');
$menu2->addLi('li4',     'titre4 avec titre sans get sans url sans css avec inlineLI'                    , NULL          ,NULL           ,NULL   ,"onclick=\"alert('alert4');\"" ,NULL             );
$menu2->addLi('li5',     'titre5 avec titre sans get avec url sans css sans inlineLI avec inlineA  '     , NULL          ,'./page5.html' ,NULL   ,"onclick=\"alert('alert5');\"" ,"target='_blank'");
echo $menu2->show();


?>
<p>Voila un menu sans style!</p>

</div><!-- //page -->

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<div name='header'>
<?php @include DOCUMENT_ROOT.'piwik.php';?></div></body></html>
