<?php
/*!******************************************************************
fichier: gestPage-0.2.php
version : 0.2
auteur : Pascal TOLEDO
date :18 avril 2012
source: http://www.legral.fr/intersites/lib/perso/php/gestPage
depend de:
	* gesLib-0.1.php
description:
	* gestion d'appel de page
tutoriel:
// ordre de surcharge1 est surcharge par 2 qui est surcharge par 3 etc
// 1- session	// $pageLast_session
// 2- cookie	// $pageLast_cook
// 3- get		// $pageLast_Get
***********************************************************************/
$gestLib->loadLib('gestPage',__FILE__,'0.2',"gestionnaire d'appel de page");

//-***********************************************************************
class gestPage
	{
	private $attr=array();
	function __construct($url)
		{
		if(!$url){return -1;}
		$this->attr['URL']=$url;
		}

	public function getAttr($attrNom){return(isset($this->attr[$attrNom])?$this->attr[$attrNom]:NULL);}
	public function setAttr($attrNom,$attrVal){if($attrNom){$this->attr[$attrNom]=$attrVal;}}

	public function showMeta()
		{
		$out='';
		if(isset($this->attr['title'])){$out.='<title>'.$this->attr['title'].'</title>';}
		if(isset($this->attr['refresh'])){$out.='<meta http-equiv="Refresh" content="'.$this->attr['refresh'].'">';}
		echo $out;
		}
	public function showA(){echo '<a href="?$var_get='.$this->attr['URL'].'">'.$this->attr['aText'].'</a>';}
	}
	
//-***********************************************************************
class gestPages
	{
	public $gestNom=NULL;
	public $pageLast=NULL;
	private $pageDefaut=NULL;	//page par defaut
	private $URL_defaut=NULL;
	public $pageActuelle=NULL;
	private $pages=NULL;		//pages (attributs)
	public $var_get=NULL;		// nom de la var utiliser dasn get ex: ?page=

	function __construct($gestNom,$pageDefaut,$URL_defaut,$var_get=NULL)
		{
		if(!($gestNom AND $pageDefaut AND $URL_defaut)){return -1;}
		$this->gestNom=$gestNom;
		$this->var_get=$var_get?$var_get:'page';
		$this->pageDefaut=$pageDefaut;		$this->URL_defaut=$URL_defaut;
		$this->pageActuelle=$this->pageDefaut;	$this->pageLast=$this->pageDefaut;
		$this->pages=array();	//page[page]->attr
		$this->addPage($pageDefaut,$URL_defaut);
		$this->setPageLast();
		$this->setPageActuelle();
		}	

	public function getPageLast(){return $this->pageLast;}
	public function setPageLast($newPageLast=NULL)
		{global $gestLib;
		if(isset($_COOKIE[$this->gestNom.'_pageActuelle'])){$this->pageLast=$_COOKIE[$this->gestNom.'_pageActuelle'];}
		if($newPageLast){$this->pageLast=$newPageLast;}
		setcookie($this->gestNom.'_pageLast',$this->pageLast,(time()+3600*24*365));
		if(isset($_COOKIE[$this->gestNom.'_pageLast'])){echo $gestLib->debugShowVar('gestPage',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'$_COOKIE['.$this->gestNom.'_pageLast]',$_COOKIE[$this->gestNom.'_pageLast']);}
		}

	public function addPage($pageNom,$url)
	     {if($pageNom AND $url){$this->pages[$pageNom]=new gestPage($url);}}

	public function getPageActuelle(){return $this->pageActuelle;}
	public function setPageActuelle($page=NULL)
		{global $gestLib;
		if($this->pageLast){$this->pageActuelle=$this->pageLast;}
		if(isset($_GET [$this->var_get])){$this->pageActuelle=$_GET [$this->var_get];}
		if(isset($_POST[$this->var_get])){$this->pageActuelle=$_POST[$this->var_get];}
		if($page){$this->pageActuelle=$page;}
		setcookie($this->gestNom.'_pageActuelle', $this->pageActuelle,(time()+3600*24*365));
		echo $gestLib->debugShowVar('gestPage',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'$this->var_get',$this->var_get);
		if(isset($_GET[$this->var_get]))
			{echo $gestLib->debugShowVar('gestPage',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'$_GET['.$this->var_get.']',$_GET[$this->var_get]);}
		echo $gestLib->debugShowVar('gestPage',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'$this->pageActuelle',$this->pageActuelle);
		echo $gestLib->debugShowVar('gestPage',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'$this->pageLast',$this->pageLast);
		if(isset($_COOKIE[$this->gestNom.'_pageActuelle'])){echo $gestLib->debugShowVar('gestPage',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'$_COOKIE['.$this->gestNom.'_pageActuelle]',$_COOKIE[$this->gestNom.'_pageLast']);}
		echo $gestLib->debugShow('gestPage',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'<br>');

		}
	public function getURL($page=NULL)
		{
		$page=$page?$page:$this->pageActuelle;
		if(@$this->pages[$page]){$url=$this->pages[$page]->getAttr('URL');return $url?$url:$this->URL_defaut;}//accee direct 'pages[$page]->getAttr' oblige!
		return NULL;
		}

	public function incPage($page=NULL)
	{global $gestLib;
	$page=$page?$page:$this->pageActuelle;
	$url=$this->getURL($page);
	if($url==NULL){$this->URL_defaut;}
	if(file_exists($url))
		{
		echo $gestLib->debugShow('gestPage',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'le fichier existe -&gt; inclusion('.$url.')');
		include $url;
		}
	elseif($this->getURL(404)){include $this->getURL(404);}
	else{include $this->URL_defaut;}
	}
	
	//Accee aux attr
	public function setAttr($page,$attrNom,$attrVal){if($page AND $attrNom){$this->pages[$page]->setAttr($attrNom,$attrVal);}}
	public function getAttr($page,$attrNom)
	     {if($page AND $attrNom)
	          {
	          if($attrNom=='URL'){return $this->getURL($page);}
	          return $this->pages[$page]->getAttr($attrNom);
	          }
		return NULL;
	     }
	public function showMeta($page=NULL)
	     {
		$page=$page?$page:$this->pageActuelle;		
		if(isset($this->pages[$page])){$this->pages[$page]->showMeta();}
	     }
	//return 1 true si une page est specifier
	public function isPageAsk()
		{
		return(isset($_GET [$this->var_get]));
		}

	public function showA($page){$this->pages[$page]->showA();}
	public function toArray($attr=NULL,$attrVal=NULL)
		{$out=array();
		if(!$attr)foreach($this->pages as $key => $value){$out[]=$key;}
		elseif(!$attrVal)//
			{
			foreach($this->pages as $key => $value){if ($value->getAttr($attr)){$out[]=$key;}}
			}
		else	{
			foreach($this->pages as $key => $value){if ($value->getAttr($attr)==$attrVal){$out[]=$key;}}
			}
		return $out;
		}
	}
$gestLib->setEtat('gestPage',LEGRAL_LIBETAT::LOADED);
//$gestPage=new gestPage('presentation',DOCUMENT_ROOT.'pages/presentation.php');
$gestLib->end('gestPage');
?>
