<?php
/*!******************************************************************
fichier: gestLib-0.1.php
version : 0.1
auteur : Pascal TOLEDO
date :01 fevrier 2012
source: http://www.legral.fr/intersites/lib/perso/php/gestLib
depend de:
	* aucune
description:
	* class de verification de librairie
	* fonction de debug avec niveau d'erreur
	* duree de parsing du fichier
documentation:
	* http://php.net/manual/fr/language.constants.predefined.php
*******************************************************************/

//********************************************
// CLASS DE GESTION DES ERREURS
//********************************************
class LEGRALERR
	{
	const ALWAYS=-1;//sera toujours afficher quelque soit le niveau atrtribuer pour la lib

	const NOERROR=0; // affiche auncune erreur
	const CRITIQUE=1;// 
	const WARNING=2; // erreur a l'execution (index de tableau invalide)
	const DEBUG=3;	// information de debuggage (suivi d'execution)
	const INFO=4;
	const ALL=5;
	}
function LEGRALERR_toString($err)
	{
	switch ($err)
		{
		case LEGRALERR::NOERROR :return 'NOERROR';
		case LEGRALERR::CRITIQUE :return 'CRITIQUE';
		case LEGRALERR::WARNING :return 'WARNING';
		case LEGRALERR::DEBUG :return 'DEBUG';
		case LEGRALERR::INFO :return 'INFO';
		case LEGRALERR::ALWAYS :return 'ALWAYS';
		}
	}

//********************************************
// CLASS DE GESTION DES ETATS
//********************************************
class LEGRAL_LIBETAT
	{
	const NOLOADED=0;
	const LOADING=1;
	const LOADED=2;
	}
function LEGRAL_LIBETAT_toString($etat)
	{
	switch ($etat)
		{
		case LEGRAL_LIBETAT::NOLOADED :return 'NOLOADED';
		case LEGRAL_LIBETAT::LOADING :return 'LOADING';
		case LEGRAL_LIBETAT::LOADED :return 'LOADED';
		}
	}


//********************************************
// CLASS DE DONNEES
//********************************************
class legral_lib
{
public $nom=NULL;
public $fichier=NULL;
public $version=NULL;
public $etat=NULL;	//1: en cours de chargement;2: chargement terminer
	function setEtat($etat){$this->etat=$etat;}
/*private*/var $dur=0;
		function getDuree(){return number_format($this->dur,6);}
/*private*/var $deb=0;
/*private*/var $fin=0;

function end()
	{
	$this->etat=LEGRAL_LIBETAT::LOADED;
	$this->fin=microtime(1);$this->dur=$this->fin - $this->deb;
	}


public $description=NULL;
public $err_level=0;
public $err_level_Backup=NULL;	//sauvegarde de l'etat
	function setErr($err){$this->err_level=$err;}
	function setErrLevelTemp($err){$this->err_level_Backup=$this->err_level;$this->err_level=$err_level;}
	function setErrLevelTemp_NOERROR(){$this->err_level_Backup=$this->err_level;$this->err_level=LEGRALERR::NOERROR;}
	function restoreErrLevel(){$this->err_level=$this->err_level_Backup;}


function __construct($nom,$file,$version,$description=NULL)
	{
	$this->deb=microtime(1);
	$this->err_level=LEGRALERR::NOERROR;
	$this->nom=$nom;
	$this->fichier=$file;
	$this->version=$version;
	$this->etat=1;
	$this->description=$description;
	}
function __toString() {return '';}

function debugShow($level,$line,$methode,$txt,$br=null)
	{
	if($level>$this->err_level){return NULL;}
	$out='<span class="legralLib_nom">'.$this->nom;
	$out.='{'.LEGRALERR_toString($level).'}';
	if (!empty($line)){$out.="($line)";}
	if (!empty($methode)){$out.="[$methode]";}
	$out.=':</span>';
	$out.='<span class="legralLib_text">'."$txt</span>";
	if (empty($br)){$out.='<br />';}
	return $out;
	}
function debugShowVar($level,$line,$methode,$varNom,$var,$br1='br')
	{
	$br1='br';

	if ($level>$this->err_level){return NULL;}
	$out='<span class="legralLib_nom">'.$this->nom;
	$out.='{'.LEGRALERR_toString($level).'}';
	if (!empty($line)){$out.="($line)";}
	if (!empty($methode)){$out.="[$methode]";}
	$out.=':</span>';
	$out.='<span class="legralLib_var">'.$varNom.'=</span>';
	$val=$var;
	if (is_object($var)){$val=print_r($val,TRUE);}
	elseif (!is_numeric($var)){$val="'$var'";};
	if (empty($var)){$val='empty';}
	//if ($var===null){$val='null';}
	if ($var===NULL){$val='NULL';}
	if ($var===0){$val=0;}
	if ($var===''){$val="''";}

//	$out.='<span class="legralLib_text">'.$val.'</span>';
//	echo "br1=$br1";
	switch ($br1)
		{
		case null:case 'nobr':$out.='<span class="legralLib_text">'.$val.'</span>';break;
		case 'br':case 1:	  $out.='<span class="legralLib_text">'.$val.'</span><br>';break;
		case 'div':		  $out.='<div class="legralLib_text">'.$val.'</div>';break;
		case 'p':			  $out.='<p class="legralLib_text">'.$val.'</p>';break;
		}
	return $out;
	}

}

//********************************************
// CLASS GESTIONLIBRAIRIE
//********************************************
class gestionLibrairie
{
public $lib=array();	//tableau de lib
//function __construct()	{	}

function loadLib($nom,$file,$version,$description=NULL)
	{
	//if ( isset($this->lib[$nom]->nom) ){return NULL;}//si deja charge
	$this->lib["$nom"]=new legral_lib($nom,$file,$version,$description);
	}
function libTableau()
	{
	$out='';
	$out.='<table><caption></caption>';
	$out.='<thead><tr><th>nom</th><th>version</th><th>etat</th><th>err level</th><th>durée</th><th>description</th></tr></thead>';
	foreach($this->lib as $key => $value)//$value= gestLib[index]
		{
		$libNom=$value->nom;
		$out.="<tr><td>$libNom</td><td>{$value->version}</td><td>".LEGRAL_LIBETAT_toString($value->etat)."</td><td>".LEGRALERR_toString($value->err_level)."</td><td>"
//		.	$this->getDuree($libNom)
		.	$this->lib[$libNom]->getDuree()
		.	"</td><td>{$value->description}</td></tr>"."\n";
		};

	$out.='</table>';
	return $out;
	}
function setEtat($lib,$etat)    {if(isset($this->lib[$lib]))$this->lib[$lib]->setEtat($etat);}
function setErr($lib,$err)	{if(isset($this->lib[$lib]))$this->lib[$lib]->setErr($err);}
function getDuree($lib)	{if(isset($this->lib[$lib]))$this->lib[$lib]->getDuree();}
function end($lib)		{$this->lib[$lib]->end();}
function getLibError($lib){if(isset($this->lib[$lib]))return LEGRALERR_toString($this->lib[$lib]->err_level);return NULL;}

function debugShow($lib,$level,$line='',$methode='',$varNom,$br=NULL)
	{if(isset($this->lib[$lib]))return $this->lib[$lib]->debugShow($level,$line,$methode,$varNom,$br=NULL);}

function debugShowVar($lib,$level,$line='',$methode='',$varNom,$var,$br=NULL)
	{if(isset($this->lib[$lib]))return $this->lib[$lib]->debugShowVar($level,$line,$methode,$varNom,$var,$br=NULL);}

}	// class gestionLibrairie

$gestLib= new gestionLibrairie();
$gestLib->loadLib('gestLib',__FILE__,'0.1','gestionnaire de librairie');
$gestLib->end('gestLib');
?>
